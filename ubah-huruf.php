<?php
    function ubah_huruf($string){
        $abjad  = "abcdefghijklmnopqrstuvwxyz";
        $output = "";

        for($i=0; $i<strlen($string); $i++){
            $pos = strpos($abjad, $string[$i]);
            $output .= substr($abjad, $pos + 1, 1);
        }
        return $output."<br>";
    }

    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu
?>